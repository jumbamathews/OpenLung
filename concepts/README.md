# Concept Proposal Process
---
## Team-based contribution
In this project, there are over 2,000 individuals contributing at any one time in various formats. With that said, there is a lot of management that has to happen in order for that to be productive. In this repository, you'll notice that our concepts directory has various concept directories within it. The requirements in each directory will be contained in of the README.md and team leads, identified by Project Management and listed in the README, can submit merge requests in order to have their progress updated to the public and rest of the project.

## Teams
| Team | Concept Directory Convention | Associated Issue Labels |
|---|---|---|
| Ventilator Hardware | ventilator-concept-x | Ventilator Hardware |
| Controller Development | ventuino-concept-x | Controller Hardware, Controller Software |
| Sensor Layout and Specifications | ventuino-sensor-concept-x | Sensor Hardware |
| Controller Housing | housing-concept-x | Controller Housing |
| Humidification Hardware | humidifier-concept-x | Humidification Hardware |

**Each team needs to keep an eye on their associated labels in issues.**

## Updating to New Concepts:
As this is a team effort, team leaders are expected to take note of the status of existing concepts, as well as talk with Project Management about what they can be doing. When you make an update to your concept, if it is a replacement of the old concept, please push the old one to the _retired-concepts directory, and name the new concept with the new concept number (x+1). Also, be sure to update the README on the new version with the information required in the README by Project Management. This helps everyone to stay in the same train of thought and allows us to better support you and update the community.

## Required fields for Concept READMEs can be found in [empty_readme.md](concepts/empty_readme.md)
